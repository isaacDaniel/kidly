<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'sequense', 'slug', 'image', 'color', 'is_active',
    ];

    public function subCategories()
    {
        return $this->hasMany(SubCategory::class);
    }
}

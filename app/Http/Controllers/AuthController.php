<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function index () 
    {
    	return view('panel.signin');
    }

    public function store (Request $req) 
    {
    	$this->validate($req, [
            'email'    => 'required|email',
            'password' => 'required',
        ]);
        if (!auth()->attempt($req->only(['email', 'password']))) {
            return redirect()->route('auth_show_signin')->withErrors(['error_auth' => 'User not found.']);
        }
        
        $user = Auth::user();
        if ($user->type == 'admin') {
            return redirect()->route('panel_index_path');            
        } else {
            return 'No eres Administrador';
        }

    }

    public function destroy ()
    {
    	auth()->logout();
        return redirect()->intended('/');
    }
}

<?php

namespace App\Http\Controllers;

use App\Category;
use App\SubCategory;
use Illuminate\Http\Request;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::with('subCategories')->get();
        return view('subcategories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $categories = Category::all();
         return view('subcategories.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        
        $validator = \Validator::make($data, [
            'name' => 'required|unique:sub_categories',
            'category_id' => 'required|exists:categories,id',
        ]);
        if ($validator->passes()) {
            $subcategory = SubCategory::create([
                'category_id' => $data['category_id'],
                'name' => $data['name'],
                'slug' => str_slug($data['name'])
            ]);
            return \Redirect::route('subcategories_index_path')
                ->with('message', 'Se ha creado la categoria: '.$data['name'].'!');
        } else {
            return \Redirect::back()->withInput()->withErrors($validator->messages());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subCategory = SubCategory::findOrFail($id);
        $categories = Category::all();

        return view('subcategories.edit', compact('categories', 'subCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $subCategory = SubCategory::findOrFail($id);
        
        $subCategory->category_id = $request->get('category_id');
        $subCategory->name = $request->get('name');
        $subCategory->slug = str_slug($request->get('name'));
        $subCategory->save();

        return \Redirect::route('subcategories_index_path')
                ->with('message', 'La subcategoría: '.$request->get('name').' se ha actualizado satisfactoriamente!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subCategory = SubCategory::findOrFail($id);
        $subCategory->delete();

        return \Redirect::route('subcategories_index_path')
                ->with('message', 'La subcategoria: '.$subCategory->name.' se han eliminado.');
    }
}

<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        return view('categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
                    'name' => 'required|unique:categories',
                    'image' => 'required|image'
                   ];
        $data = $request->all();
        $data['slug'] = str_slug($request->get('name'));
        $data['is_active'] = \Config::get('constants.ACTIVE');
        if ($lastCategory = Category::all()->last()) {
            $data['sequense'] = $lastCategory->sequense + 1;            
        } else {
            $data['sequense'] = 1;
        }
        $validator = \Validator::make($data, $rules);
        if ($validator->passes()) {
            $category = Category::create([
                'name' => $data['name'],
                'sequense' => $data['sequense'],
                'slug' => $data['slug'],
                'image' => $request->file('image')->store('categories'), 
                'color' => $data['color'],
                'is_active' => $data['is_active']
            ]);
            return \Redirect::route('categories_index_path')
                ->with('message', 'Se ha creado la categoria: '.$data['name'].'!');
        } else {
            return \Redirect::back()->withInput()->withErrors($validator->messages());
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);

        return view('categories.edit', compact('category', 'image'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::findOrFail($id);
        if ($request->file('image')) {
            \Storage::delete($category->image);
            $category->image = $request->file('image')->store('categories');
        }
        $category->name = $request->get('name');
        $category->sequense = $request->get('sequense');
        $category->slug = str_slug($request->get('name'));
        $category->color = $request->get('color');
        $category->is_active = ($request->get('is_active')) ? \Config::get('constants.ACTIVE') : \Config::get('constants.INACTIVE');
        $category->save();

        return \Redirect::route('categories_index_path')
                ->with('message', 'La categoria: '.$request->get('name').' se ha actualizado satisfactoriamente!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        $category->delete();

        return \Redirect::route('categories_index_path')
                ->with('message', 'La categoria: '.$category->name.' y sus subcategorias se han eliminado.');
    }
}

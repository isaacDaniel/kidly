<?php 

namespace App\Providers;

use App\Composers\CurrentUserComposer;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
	public function boot(Factory $factory)
    {
        $factory->composer('*', CurrentUserComposer::class);
    }
    
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // TODO: Implement register() method.
    }
}


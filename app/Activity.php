<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $fillable = [];

    public function category() 
    {
    	return $this->belongsTo(Category::class);
    }
    public function gallery() 
    {
    	return $this->belongsTo(Gallery::class);
    }
    public function user() 
    {
    	return $this->belongsTo(User::class);
    }
}

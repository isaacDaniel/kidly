<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    echo 'Este es el home!!!';
});

Route::get('/login', function () {
    echo 'Logeate';
});

Route::get('panel/signin', [
	'uses' => 'AuthController@index', 
	'as'   => 'auth_show_signin',
]);

Route::post('panel/signin', [
	'uses' => 'AuthController@store', 
	'as'   => 'auth_store_signin',
]);

Route::get('panel/signout', [
    'uses' => 'AuthController@destroy',
    'as'   => 'auth_destroy_signout',
]);

Route::group(['middleware' => 'auth'], function () {
	Route::get('panel/index', [
		'uses' => 'PanelController@index', 
		'as'   => 'panel_index_path',
	]);
	Route::get('panel/categories', [
		'uses' => 'CategoryController@index',
		'as'   => 'categories_index_path',
	]);
	Route::resource('panel/category', 'CategoryController', 
		['names'  => ['store'   => 'category_store_path',
					  'create'  => 'category_create_path',
					  'update'  => 'category_update_path',
					  'destroy' => 'category_destroy_path',
					  'show'    => 'category_show_path',
					  'edit'    => 'category_edit_path',],
		'except' => ['index', ],
		'parameters' => ['category' => 'id'],
	]);
	Route::get('panel/sub-categories', [
		'uses' => 'SubCategoryController@index',
		'as'   => 'subcategories_index_path',
	]);
	Route::resource('panel/sub-category', 'SubCategoryController', 
		['names'  => ['store'   => 'subcategory_store_path',
					  'create'  => 'subcategory_create_path',
					  'update'  => 'subcategory_update_path',
					  'destroy' => 'subcategory_destroy_path',
					  'show'    => 'subcategory_show_path',
					  'edit'    => 'subcategory_edit_path',],
		'except' => ['index', ],
		'parameters' => ['sub-category' => 'id'],
	]);

	Route::get('panel/activities', [
		'uses' => 'ActivityController@index',
		'as'   => 'activities_index_path',
	]);
	Route::resource('panel/activity', 'ActivityController', 
		['names'  => ['store'   => 'activity_store_path',
					  'create'  => 'activity_create_path',
					  'update'  => 'activity_update_path',
					  'destroy' => 'activity_destroy_path',
					  'show'    => 'activity_show_path',
					  'edit'    => 'activity_edit_path',],
		'except' => ['index', ],
		'parameters' => ['activity' => 'id'],
	]);
});


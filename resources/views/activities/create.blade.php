@extends('layouts.panel')
@section('content')
	<div class="panel panel-default">
		<div class="panel panel-body">
			<form>
				<div class="col-sm-8">
					<div class="form-group">
						<label>Titulo</label>
						<input type="text" class="form-control" name="title" placeholder="Titulo">
					</div>
					<div class="form-group">
						<label>Descripcion</label>
						<textarea rows="25" class="textarea-summernote" name="description" placeholder="Descripción"></textarea>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							Tags
						</div>
						<div class="panel-body">
							<div class="input-group">
								<input type="text" name="tag" placeholder="Escribe un tag" class="form-control">
								<span class="input-group-btn">
							    	<button class="btn btn-default" type="button">Añadir</button>
							    </span>
							</div>
							<span class="text-muted">Separa las etiquetas con comas.</span>								
							<div id="tags"></div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							Edades
						</div>
						<div class="panel-body">
							<div class="col-sm-2">
								<strong>
									Selecciona las edades aptas para el evento
								</strong>
								<span>
									Puedes escojer mas de una opción
								</span>
							</div>
							<div class="col-sm-8 col-sm-offset-2">
								<label>
									<input type="checkbox">
									1 menos
								</label>
								<label>
									<input type="checkbox">
									1 año
								</label>
								<label>
									<input type="checkbox">
									2 años
								</label>
								<label>
									<input type="checkbox">
									3 años
								</label>
								<label>
									<input type="checkbox">
									4 años
								</label>
								<label>
									<input type="checkbox">
									5 años
								</label>
								<label>
									<input type="checkbox">
									6 años
								</label>								
							</div>
							<div class="col-sm-8 col-sm-offset-2">
								<label>
									<input type="checkbox">
									7 años
								</label>
								<label>
									<input type="checkbox">
									8 años
								</label>
								<label>
									<input type="checkbox">
									9 años
								</label>
								<label>
									<input type="checkbox">
									10 años
								</label>
								<label>
									<input type="checkbox">
									11 años
								</label>
								<label>
									<input type="checkbox">
									12 años
								</label>
								<label>
									<input type="checkbox">
									Todas
								</label>								
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							Galería de imágenes
						</div>
						<div class="panel-body">
							<img class="img-responsive" src="http://placehold.it/750x500">
							<div class="row">
					            <div class="col-lg-12">
					                <h3 class="page-header">Relacionadas</h3>
					            </div>
					            <div class="col-sm-3 col-xs-6">
					                <a href="#">
					                    <img class="img-responsive portfolio-item" src="http://placehold.it/300x200" alt="">
					                </a>
					            </div>
					            <div class="col-sm-3 col-xs-6">
					                <a href="#">
					                    <img class="img-responsive portfolio-item" src="http://placehold.it/300x200" alt="">
					                </a>
					            </div>
					            <div class="col-sm-3 col-xs-6">
					                <a href="#">
					                    <img class="img-responsive portfolio-item" src="http://placehold.it/300x200" alt="">
					                </a>
					            </div>
					            <div class="col-sm-3 col-xs-6">
					                <a href="#">
					                    <img class="img-responsive portfolio-item" src="http://placehold.it/300x200" alt="">
					                </a>
					            </div>
					        </div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							Horarios
						</div>
						<div class="panel-body">
							<button class="btn btn-success">Agregar un nuevo horario</button>
							<table class="table">
								<tbody>
									<tr>
										<th>Nombre del lugar</th>
										<th>15 de Marzo 2017</th>
										<th>14:15 - 15:15 hrs</th>
										<th>
											<a href="">Remover</a>
											<a href="">Ver</a>
										</th>
									</tr>
								</tbody>
							</table>
						</div>
					</div>				
				</div>
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							Publicar
						</div>
						<div class="panel-body">
							<select name="status" class="form-control">
								<option value="DRAF">Borrador</option>
								<option value="VALIDATE">Pendiente</option>
								<option value="PRODUCTION">Publicado</option>
							</select>
							<span>Guardar como Borrador</span>
							<p>
								Actualmente la actividad esta guardada en formato <strong>"Borador"</strong>, no es visible publicamente, cambie a <strong>"Publicada"</strong> para hacerla visible.
							</p>
							<input type="button" class="btn btn-danger" value="Borrar actividad">
							<input type="button" class="btn btn-primary" value="Guardar cambios">
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							Formato
						</div>
						<div class="panel-body">
							<div class="form-group">
								<select name="type" class="form-control">
									<option value="PREMIUM">Premium</option>
									<option value="MEDIUM">Medium</option>
									<option value="GENERAL">General</option>
								</select>								
							</div>
							<div class="form-check">
					          	<label class="form-check-label">
									<input class="form-check-input" name="activate_box_buy" type="checkbox"> Activar el cuadro de compras
								</label>
					        </div>							
							<div class="form-group">
								<label>Titulo del recuadro</label>
								<input class="form-control" type="text" name="title_box" placeholder="Título del recuadro">
							</div>
							<div class="form-group">
								<label>Descripcion</label>
								<textarea class="form-control" name="description_box" placeholder="Texto del recuadro"></textarea>
							</div>
							<div class="form-group">
								<label>Link</label>
								<input class="form-control" type="text" name="link_box" placeholder="Agrege el link del boton">
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							Ubicaciones
						</div>
						<div class="panel-body">
							<div class="form-check">
							  <label class="form-check-label">
							    <input class="form-check-input" type="checkbox" value=""> Ubicación 1
							  </label>
							</div>
							<div class="form-check">
							  <label class="form-check-label">
							    <input class="form-check-input" type="checkbox" value=""> Ubicación 2
							  </label>
							</div>
							<div class="form-check">
							  <label class="form-check-label">
							    <input class="form-check-input" type="checkbox" value=""> Ubicación 3
							  </label>
							</div>
							<button class="btn btn-success">Nueva Ubicación</button>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							Categoría
						</div>
						<div class="panel-body">
							<div class="form-group">
								<select class="form-control" name="category">
									<option value="">Espectaculos</option>
								</select>
								<span class="text-muted">Seleccione las subcategorias para la categoria <strong>"Espectaculos"</strong></span>
							</div>
							<div class="form-check form-check-inline">
							  	<label class="form-check-label">
							    	<input name="subcategory_id" class="form-check-input" type="checkbox" value=""> Teatro
							  	</label>
							</div>
							<div class="form-check form-check-inline">
							  	<label class="form-check-label">
							    	<input name="subcategory_id" class="form-check-input" type="checkbox" value=""> Cine
							  	</label>
							</div>
							<div class="form-check form-check-inline">
							  	<label class="form-check-label">
							    	<input name="subcategory_id" class="form-check-input" type="checkbox" value=""> Musicales
							  	</label>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							Vigencia del evento
						</div>
						<div class="panel-body">
							<div class="form-check">
							  	<label class="form-check-label">
							    	<input name="validity" class="form-check-input" type="checkbox" value=""> Esta actividad solo se mostrará en la plataforma mientras tenga vigencia valida
							  	</label>
							</div>
							<span>Ingrese una vigencia del evento</span>
							<div class="col-sm-12">
								<div class="col-sm-6 form-group">
					                <div class='input-group date timepicker'>
					                    <input name="validity_fech_ini" type='text' class="form-control" />
					                    <span class="input-group-addon">
					                        <span class="glyphicon glyphicon-calendar"></span>
					                    </span>
					                </div>
					            </div>
					            <div class="col-sm-6 form-group">
		                            <div class='input-group date timepicker'>
		                                <input name="validity_fech_fin" type='text' class="form-control" />
		                                <span class="input-group-addon">
		                                    <span class="glyphicon glyphicon-calendar"></span>
		                                </span>
		                            </div>
		                        </div>								
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							Actividad Recomendada
						</div>
						<div class="panel-body">
							<div class="form-check">
							  	<label class="form-check-label">
							    	<input name="recommend" class="form-check-input" type="checkbox" value=""> Mostrar esta actividad como recomendada
							  	</label>
							</div>
							<span>Ingrese una vigencia para aparecer como recomendada</span>
							<div class="col-sm-12">
								<div class="col-sm-6 form-group">
					                <div class='input-group date timepicker'>
					                    <input name="recommend_fech_ini" type='text' class="form-control" />
					                    <span class="input-group-addon">
					                        <span class="glyphicon glyphicon-calendar"></span>
					                    </span>
					                </div>
					            </div>
					            <div class="col-sm-6 form-group">
		                            <div class='input-group date timepicker'>
		                                <input name="recommend_fech_ini" type='text' class="form-control" />
		                                <span class="input-group-addon">
		                                    <span class="glyphicon glyphicon-calendar"></span>
		                                </span>
		                            </div>
		                        </div>								
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							Liston en publicación
						</div>
						<div class="panel-body">
							<div class="form-check">
							  	<label class="form-check-label">
							    	<input name="lath" class="form-check-input" type="checkbox" value=""> Mostrar esta actividad con liston
							  	</label>
							</div>
							<div class="form-group">
								<label>Texto para liston</label>
								<input name="description_lath" type="text" class="form-control" placeholder="Ingrese el texto para el liston">
							</div>
							<div class="form-group">
								<label>
									<input type="color" name="color_lath" value="#AACD05"> Seleccione un color para el liston
								</label>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							Imagen destacada
						</div>
						<div class="panel-body">
							<img class="img-responsive portfolio-item" src="http://placehold.it/500x300" alt="">
						</div>
						<span class="text-muted">Pulse en la imagen para editarla o actualizarla</span>
						<br>
						<a href="javascript:void(0);">Quitar la imagen destacada</a>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							Skyn
						</div>
						<div class="panel-body">
							<div class="form-check">
							  	<label class="form-check-label">
							    	<input name="skyn" class="form-check-input" type="checkbox" value=""> Activar Skyn en esta actividad
							  	</label>
							</div>
							<a href="javascript:void(0)">Haz click aqui para subir la imagen de skyn</a>
							<br>
							<span>Ingrese una vigencia del evento</span>
							<div class="col-sm-12">
								<div class="col-sm-6 form-group">
					                <div class='input-group date timepicker'>
					                    <input name="skyn_fech_ini" type='text' class="form-control" />
					                    <span class="input-group-addon">
					                        <span class="glyphicon glyphicon-calendar"></span>
					                    </span>
					                </div>
					            </div>
					            <div class="col-sm-6 form-group">
		                            <div class='input-group date timepicker'>
		                                <input name="skyn_fech_fin" type='text' class="form-control" />
		                                <span class="input-group-addon">
		                                    <span class="glyphicon glyphicon-calendar"></span>
		                                </span>
		                            </div>
		                        </div>								
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>		
	</div>
@stop
@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/summernote.css') }}">
@stop
@section('js')
	<script type="text/javascript" src="{{ asset('js/summernote.min.js') }}"></script>	
	<script type="text/javascript">
	    $(function() {
	        $('.textarea-summernote').summernote({
	        	height: 250,
	        	lang: 'es-ES',
	        });

	        // No esta instalado el datetimepicker
	        // $('.timepicker').datetimepicker();
	    });
	</script>
@stop
<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 style="color:{{ $colorIcon }};"><span class="fa {{ $iconType }}"></span> <span class="titulo"></span></h4>
            </div>
            <form id="form-modal" role="form" action="" method="post">
            	{{ $method }}
                <div class="modal-body">
                        <p class="descripcion"></p>
                        <p class="pregunta"></p>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
                    <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-trash"></span> {{ $labelConfirmation }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
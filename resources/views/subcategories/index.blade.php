@extends('layouts.panel')
@section('content')
	@if (Session::has('message'))
	    @component('components.alert', ['alertType' => 'alert-info'])
			@slot('message')
				{{ Session::get('message') }}
			@endslot
	    @endcomponent
	@endif
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h1 class="panel-title">Subcategorias</h1>			
		</div>
		<div class="panel-body">
		@foreach ($categories as $category)
			@if (count($category->subCategories) > 0)
				<div class="col-sm-12">
					<h3 class="page-header">{{ $category->name }}</h3>
					<table class="table table-responsive table-striped">
						<thead>
							<tr>
								<th>#</th>
								<th>Subcategoría</th>
								<th>Acciones</th>
							</tr>
						</thead>
						<tbody>
						@foreach ($category->subCategories as $key => $sub)
							<tr>
								<th>{{ $key + 1 }}</th>
								<th>{{ $sub->name }}</th>
								<th>
									<a href="{{ route('subcategory_edit_path', ['id' => $sub->id]) }}">Editar</a>
									<a class="delete-subcategory" href="{{ route('subcategory_destroy_path', ['id' => $sub->id]) }}">Eliminar</a> 
								</th>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
			@endif
		@endforeach
		</div>
	</div>
	@component('components.modal', ['colorIcon' => 'red', 'iconType' => 'fa-exclamation-triangle'])
	    @slot('method')
	        {{ method_field('DELETE') }}
	        {{ csrf_field() }}
	    @endslot
	    @slot('labelConfirmation')
			Sí, eliminar.
	    @endslot
	@endcomponent
@stop
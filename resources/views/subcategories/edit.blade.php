@extends('layouts.panel')
@section('content')
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h1 class="panel-title">Editar</h1>			
		</div>
		<div class="panel-body">
			<form action="{{ route('subcategory_update_path', ['id' => $subCategory->id]) }}" method="post">
				{{ method_field('PUT') }}
				{{ csrf_field() }}
				<div class="form-group">
					<label>Categoría</label>
					<select name="category_id" class="form-control">
						<option>Selecciona una categoria</option>
						@foreach ($categories as $category)
							<option value="{{ $category->id }}" {{ ($subCategory->category_id == $category->id) ? 'selected="selected"' : ''}}>{{ $category->name }}</option>
						@endforeach
					</select>
					@if ($errors->has('category_id'))
                        <span class="help-block">
	                        <strong>{{ $errors->first('category_id') }}</strong>
	                    </span>
                    @endif
				</div>
				<div class="form-group">
					<label>Subcategoría</label>
					<input name="name" class="form-control" value="{{ (old('name')) ? old('name') : $subCategory->name }}">
					@if ($errors->has('name'))
                        <span class="help-block">
                        	<strong>{{ $errors->first('name') }}</strong>
                    	</span>
                    @endif
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-primary" value="Crear">
				</div>
			</form>
		</div>
	</div>
@stop
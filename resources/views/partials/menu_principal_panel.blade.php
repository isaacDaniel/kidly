<!-- Sidebar -->
<div id="sidebar-wrapper">
    {{-- <p>
        Welcome: <strong>{{ $currentUser->name }}</strong>
    </p> --}}
    <ul class="sidebar-nav">
        <li class="sidebar-brand">
            <a href="/">
                Kidly
            </a>
        </li>
        <li class="submenu">
            <a tabindex="-1" href="javascript:void(0);" >Categorias</a>
            <ul>
                <li><a href="{{ route('categories_index_path') }}">Listar</a></li>
                <li><a href="{{ route('category_create_path') }}">Crear</a></li>
                {{-- <li><a href="#"></a></li> --}}
            </ul>
        </li>
        <li class="submenu">
            <a tabindex="-1" href="javascript:void(0);" >Sub Categorias</a>
            <ul>
                <li><a href="{{ route('subcategories_index_path') }}">Listar</a></li>
                <li><a href="{{ route('subcategory_create_path') }}">Crear</a></li>
            </ul>
        </li>
        <li class="submenu">
            <a tabindex="-1" href="javascript:void(0);" >Actividades</a>
            <ul>
                <li><a href="{{ route('activities_index_path') }}">Listar</a></li>
                <li><a href="{{ route('activity_create_path') }}">Crear</a></li>
            </ul>
        </li>
        <li>
            <a href="#">Events</a>
        </li>
        <li>
            <a href="#">About</a>
        </li>
        <li>
            <a href="#">Services</a>
        </li>
        <li>
            <a href="#">Contact</a>
        </li>
        <li>
            <a href="{{ route('auth_destroy_signout') }}">Logout</a>
        </li>
    </ul>
</div>
<!-- /#sidebar-wrapper -->
<nav class="navbar navbar-toggleable-md navbar-inverse bg-inverse" >
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="javascript:void(0);">Welcome: <strong>{{ $currentUser->name }}</strong></a>
            </li>
        </ul>
    </div>
</nav>
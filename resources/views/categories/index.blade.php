@extends('layouts.panel')
@section('content')
	<h1>Categorías</h1>
	@if (Session::has('message'))
	    @component('components.alert', ['alertType' => 'alert-info'])
			@slot('message')
				{{ Session::get('message') }}
			@endslot
	    @endcomponent
	@endif
	<table class="table table-striped table-responsive">
		<thead>
			<tr>
				<th>#</th>
				<th>Categoria</th>
				<th>Color</th>
				<th>Imagen</th>
				<th>Activo</th>
				<th>Acciones</th>
			</tr>
		</thead>
		<tbody>
		@foreach ($categories as $key => $category)
			<tr>
				<th>{{ $key + 1 }}</th>
				<th>{{ $category->name }}</th>
				<th>{{ $category->color }}</th>
				<th>{{ $category->image }}</th>
				<th>{{ ($category->is_active) ? 'Si' : 'No' }}</th>
				<th>
					{{-- <a href="javascript:void(0);">{{ ($category->is_active) ? 'Desactivar' : 'Activar' }}</a>  --}}
                    <a href="{{ route('category_edit_path', ['id' => $category->id]) }}">Editar</a>  
                    <a class="delete-category" href="{{ route('category_destroy_path', ['id' => $category->id]) }}">Eliminar</a>  
				</th>
			</tr>
		@endforeach		
		</tbody>		
	</table>
	@component('components.modal', ['colorIcon' => 'red', 'iconType' => 'fa-exclamation-triangle'])
	    @slot('method')
	        {{ method_field('DELETE') }}
	        {{ csrf_field() }}
	    @endslot
	    @slot('labelConfirmation')
			Sí, eliminar.
	    @endslot
	@endcomponent	
@stop

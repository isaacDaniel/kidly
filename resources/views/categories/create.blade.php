@extends('layouts.panel')
@section('content')
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h1 class="panel-title">Crear una categoría</h1>			
		</div>
		<div class="panel-body">
			<form action="{{ route('category_store_path') }}" method="post" enctype="multipart/form-data">
				{{ csrf_field() }}
				<div class="form-group">
					<label>Nombre</label>
					<input name="name" class="form-control" value="{{ old('name') }}">
					@if ($errors->has('name'))
                        <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
				</div>
				<label>Color</label>
				<div id="colp" data-format="alias" class="form-group input-group colorpicker-component">
				    <input type="text" value="{{ (old('color')) ? old('color') : 'primary' }}" class="form-control" name="color" />
				    <span class="input-group-addon"><i></i></span>
				</div>
				<div class="form-group">
					<label>Imagen</label>
					<input name="image" type="file">
					@if ($errors->has('image'))
                        <span class="help-block">
                        <strong>{{ $errors->first('image') }}</strong>
                    </span>
                    @endif
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-primary" value="Crear">
				</div>
			</form>
		</div>
	</div>
@stop
@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-colorpicker.min.css') }}">
@stop
@section('js')
	<script type="text/javascript" src="{{ asset('js/bootstrap-colorpicker.min.js') }}"></script>	
	<script type="text/javascript">
	    $(function() {
	        $('#colp').colorpicker({
	            colorSelectors: {
	                'black': '#000000',
	                'white': '#ffffff',
	                'red': '#FF0000',
	                'default': '#777777',
	                'primary': '#337ab7',
	                'success': '#5cb85c',
	                'info': '#5bc0de',
	                'warning': '#f0ad4e',
	                'danger': '#d9534f'
	            }
	        });
	    });
	</script>
@stop
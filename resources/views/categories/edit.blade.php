@extends('layouts.panel')
@section('content')
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h1 class="panel-title">Editar</h1>			
		</div>
		<div class="panel-body">
			<form action="{{ route('category_update_path', ['id' => $category->id]) }}" method="post" enctype="multipart/form-data">
				{{ method_field('PUT') }}
				{{ csrf_field() }}
				<div class="form-group">
					<label>Nombre</label>
					<input name="name" class="form-control" value="{{ (old('name')) ? old('name') : $category->name }}">
					@if ($errors->has('name'))
                        <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
				</div>
				<div class="form-group">
					<label>Orden</label>
					<input name="sequense" class="form-control" value="{{ (old('sequense')) ? old('sequense') : $category->sequense }}">
					@if ($errors->has('sequense'))
                        <span class="help-block">
                        <strong>{{ $errors->first('sequense') }}</strong>
                    </span>
                    @endif
				</div>
				<label>Color</label>
				<div id="colp" data-format="alias" class="form-group input-group colorpicker-component">
				    <input type="text" class="form-control" name="color" value="{{ (old('color')) ? old('color') : $category->color }}" />
				    <span class="input-group-addon"><i></i></span>
				</div>
				<div class="form-group col-sm-12">
					<img class="current_image" src="{{ asset('storage/'.$category->image) }}" width="220" />			            
		          	<a href="javascript:void(0);" class="current_image btn btn-default btn-changeImage" ><i class="fa fa-pencil-square-o"> Cambiar</i></a>
					<input id="btn-new_image" name="image" type="file" style="display: none">
					@if ($errors->has('image'))
                        <span class="help-block">
                        <strong>{{ $errors->first('image') }}</strong>
                    </span>
                    @endif
				</div>
				<div class="form-group col-sm-12">
					<label>
						<input name="is_active" type="checkbox" {{ ($category->is_active != 0) ? 'checked' : '' }}> Activo
					</label>
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-primary" value="Actualizar">
				</div>
			</form>
		</div>
	</div>
@stop
@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-colorpicker.min.css') }}">
@stop
@section('js')
	<script type="text/javascript" src="{{ asset('js/bootstrap-colorpicker.min.js') }}"></script>	
	<script type="text/javascript">
	    $(function() {
	        $('#colp').colorpicker({
	            colorSelectors: {
	                'black': '#000000',
	                'white': '#ffffff',
	                'red': '#FF0000',
	                'default': '#777777',
	                'primary': '#337ab7',
	                'success': '#5cb85c',
	                'info': '#5bc0de',
	                'warning': '#f0ad4e',
	                'danger': '#d9534f'
	            }
	        });
	    });
	</script>
@stop
@extends('layouts.panel')
@section('pag_title', 'Sign In')
@section('content')
	<h1>Iniciar Sesión</h1>
	@if($errors->has('error_auth'))
	    <div class="alert alert-danger">
			{{ $errors->first('error_auth') }}	        
	    </div>
	@endif
	<form method="post" action="{{ route('auth_store_signin') }}" >
		{{ csrf_field() }}
		<label>Correo:</label>
		<input type="email" placeholder="correo@example.com" class="form-control" name="email" />
		@if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
		<label>Contraseña</label>
		<input type="password" class="form-control" name="password" />
		@if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
		<input type="submit" value="Entrar" class="btn btn-primary">
	</form>
@stop
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, shrink-to-fit=no, initial-scale=1">
    <meta name="description" content="Administracion Kidly">
    <meta name="author" content="Denumeris Interactive">
    <title>@yield('pag_title', 'Panel de Administración') - Kidly</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/simple-sidebar.css') }}">
	@yield('css')
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<section id="wrapper">
		@if($currentUser)
        	@include('partials.menu_principal_panel')
    	@endif
    	<div id="page-content-wrapper">
    		<div class="container-fluid">
    			<div class="row">
					@yield('content')    				
    			</div>
    		</div>
    	</div>
	</section>
	<footer>
		<script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/tether.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
		<script>
	    $("#menu-toggle").click(function(e) {
	        e.preventDefault();
	        $("#sidebar-wrapper").toggleClass("toggled");
	    });
	    </script>
		@yield('js')
		<script type="text/javascript" src="{{ asset('js/kidly.admin.js') }}"></script>		
	</footer>
</body>
</html>
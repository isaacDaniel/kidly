( function() {

	// change image of a Category
	$('.btn-changeImage').on('click', function(event) {
		event.preventDefault();
		$('.current_image').hide('slow');
		$('#btn-new_image').show('slow');
	});

	//Delete category
	$('.delete-category').on('click', function(event) {    
         event.preventDefault();
         var $link = $(this).attr('href');
         $('span.titulo').html('Eliminar categoria');
         $('p.descripcion').html('Está a punto de eliminar esta categoria y todo su contenido.');
         $('p.pregunta').html('¿Seguro desea eliminar la categoria?');
         var action = $('#form-modal').attr('action', $link);
         $("#myModal").modal();
    });

    $('.delete-subcategory').on('click', function(event) {    
         event.preventDefault();
         var $link = $(this).attr('href');
         $('span.titulo').html('Eliminar Subcategoría');
         $('p.descripcion').html('Está a punto de eliminar esta subcategoría.');
         $('p.pregunta').html('¿Seguro desea eliminar la subcategoría?');
         var action = $('#form-modal').attr('action', $link);
         $("#myModal").modal();
    });
})()
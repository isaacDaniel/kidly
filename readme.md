## KIDLY - Laravel

This is a repository of Kidly
author: [Isaac Daniel Batista](dbautista@denumeris.com).

## License
The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
